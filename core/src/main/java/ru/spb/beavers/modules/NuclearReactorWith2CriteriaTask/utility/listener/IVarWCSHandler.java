package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener;

public interface IVarWCSHandler {
    public void rminChanged();
    public void rmaxChanged();
    public void betaChanged();
}
