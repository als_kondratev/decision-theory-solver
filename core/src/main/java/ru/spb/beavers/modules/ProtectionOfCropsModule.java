package ru.spb.beavers.modules;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import ru.spb.beavers.modules.protection_of_crops.Constants;


public class ProtectionOfCropsModule implements ITaskModule {
    private final JTextField v1TextField;
    private final JTextField v2TextField;
    private final JTextField v3TextField;
    private final JTextField v4TextField;
    private final JTextField p1TextField;
    private float v1;
    private float v2;
    private float v3;
    private float v4;
    private float ud1;
    private float ud2;
    private float pdef;
    private float p1;

    private boolean isValidTextValue(String text) {
        Pattern p = Pattern.compile("\\-?\\d+(\\.\\d{0,})?", Pattern.UNICODE_CASE);
        return p.split(text).length == 0;
    }

    private boolean checkParms() {
        if(!isValidTextValue(v1TextField.getText())) return false;
        if(!isValidTextValue(v2TextField.getText())) return false;
        if(!isValidTextValue(v3TextField.getText())) return false;
        if(!isValidTextValue(v4TextField.getText())) return false;
        if(!isValidTextValue(p1TextField.getText())) return false;
        return true;
    }

    private String modifyExampleLine(String line) {
        String [] items = line.split(" ");
        String result = "";
        for(String item : items) {
            switch(item) {
                case "v1": result += v1 + " "; break;
                case "v2": result += v2 + " "; break;
                case "v3": result += v3 + " "; break;
                case "v4": result += v4 + " "; break;
                case "p1": result += p1 + " "; break;
                case "pdef": result += pdef + " "; break;
                case "ud1": result += ud1 + " "; break;
                case "ud2": result += ud2 + " "; break;
                default: result += item + " ";
            }
        }
        return result;
    }

    String getResultTailWithAddInfo() {
        String tail = "<b>Результат:</b><br/>";
        if(ud1 > ud2) {
            tail +=
                    "Фермер решит защищать посевы от заморозков т. к. " +
                            "u(d<sub>1</sub>) = " + ud1 + " больше чем u(d<sub>2</sub>) = " + ud2 + ".<br/><br/>";
        } else {
            tail += "Фермер решит не защищать посевы, т. к. " +
                    "u(d<sub>1</sub>) = " + ud1 + " не больше чем u(d<sub>2</sub>) = " + ud2 + ".<br/><br/>";
        }
        tail += "</p><br/><br/></html>";
        return tail;
    }

    public ProtectionOfCropsModule() {
        v1TextField = new JTextField("50");
        v2TextField = new JTextField("75");
        v3TextField = new JTextField("-50");
        v4TextField = new JTextField("100");
        p1TextField = new JTextField("0.33");

        v1TextField.setMaximumSize(Constants.TEXT_FIELD_SIZE);
        v2TextField.setMaximumSize(Constants.TEXT_FIELD_SIZE);
        v3TextField.setMaximumSize(Constants.TEXT_FIELD_SIZE);
        v4TextField.setMaximumSize(Constants.TEXT_FIELD_SIZE);
        p1TextField.setMaximumSize(Constants.TEXT_FIELD_SIZE);

        v1TextField.setAlignmentX(Container.CENTER_ALIGNMENT);
        v2TextField.setAlignmentX(Container.CENTER_ALIGNMENT);
        v3TextField.setAlignmentX(Container.CENTER_ALIGNMENT);
        v4TextField.setAlignmentX(Container.CENTER_ALIGNMENT);
        p1TextField.setAlignmentX(Container.CENTER_ALIGNMENT);
    }

    @Override
    public String getTitle() {
        return Constants.TITLE;
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        String descriptionText = "";

        // Get the HTML based description text from resources.
        try {
            URL resource = ClassLoader.getSystemClassLoader().getResource(
                    Constants.RESOURCE_PATH + Constants.DESCRIPTION_FILE
            );
            assert resource != null;
            Path path = Paths.get(resource.toURI());
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) descriptionText += line;
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(ProtectionOfCropsModule.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Set up description context.
        JLabel descriptionLabel = new JLabel(descriptionText);
        descriptionLabel.setPreferredSize(new Dimension(770,400));
        panel.removeAll();
        panel.add(descriptionLabel);

    }

    @Override
    public void initSolutionPanel(JPanel panel) {
        String solutionText1 = "";
        String solutionText2 = "";
        URL resource;
        // Get the HTML based description text from resources.
        try {
            resource = ClassLoader.getSystemClassLoader().getResource(
                    Constants.RESOURCE_PATH + Constants.SOLUTION_FILE1
            );
            Path path = Paths.get(resource.toURI());
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) solutionText1 += line;

            resource = ClassLoader.getSystemClassLoader().getResource(
                    Constants.RESOURCE_PATH + Constants.SOLUTION_FILE2
            );
            path = Paths.get(resource.toURI());
            lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) solutionText2 += line;
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(ProtectionOfCropsModule.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Set up solution context.
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JLabel solutionLabel1 = new JLabel(solutionText1);
        solutionLabel1.setMinimumSize(new Dimension(770, 400));
        solutionLabel1.setMaximumSize(new Dimension(770,400));
        panel.add(solutionLabel1);
        resource = ClassLoader.getSystemClassLoader().getResource(
                Constants.RESOURCE_PATH + Constants.TREE_FILE
        );
        panel.add(new JLabel(new ImageIcon(resource)));


        panel.add(new JLabel(Constants.DIAGRAM_TEXT));
        resource = ClassLoader.getSystemClassLoader().getResource(
                Constants.RESOURCE_PATH + Constants.DIAGRAM_FILE
        );
        panel.add(new JLabel(new ImageIcon(resource)));
        JLabel solutionLabel2 = new JLabel(solutionText2);
        solutionLabel2.setMinimumSize(new Dimension(770, 400));
        solutionLabel2.setMaximumSize(new Dimension(770,400));
        panel.add(solutionLabel2);
    }

    @Override
    public void initInputPanel(JPanel panel) {
        // Make up input panel using existings final gui items.
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JLabel title = new JLabel(Constants.INPUT_PANEL_TITLE);
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        title = new JLabel(Constants.V1_INPUT_STRING);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        panel.add(v1TextField);
        title = new JLabel(Constants.V2_INPUT_STRING);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        panel.add(v2TextField);
        title = new JLabel(Constants.V3_INPUT_STRING);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        panel.add(v3TextField);
        title = new JLabel(Constants.V4_INPUT_STRING);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        panel.add(v4TextField);
        title = new JLabel(Constants.P1_INPUT_STRING);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        panel.add(p1TextField);
    }

    @Override
    public void initExamplePanel(JPanel panel) {
        panel.removeAll();
        //panel.setLayout(new GridLayout(2, 1));
        // Check if we have valid parms
        if(!checkParms()) {
            JLabel errorText = new JLabel(Constants.PARMS_ERROR_STRING);
            panel.add(errorText);
            return;
        }
        // Set up context
        v1 = Float.parseFloat(v1TextField.getText());
        v2 = Float.parseFloat(v2TextField.getText());
        v3 = Float.parseFloat(v3TextField.getText());
        v4 = Float.parseFloat(v4TextField.getText());
        p1 = Float.parseFloat(p1TextField.getText());
        ud1 = (float) (p1 * v1 + (1 - p1) * v2);
        ud1 = (float) (p1 * v3 + (1 - p1) * v4);
        pdef = (v4 - v2) / (v1 - v3 + v4 - v2);

        String exampleText = "";
        // Get template
        try {
            URL resource = ClassLoader.getSystemClassLoader().getResource(
                    Constants.RESOURCE_PATH + Constants.EXAPMLE_FILE
            );
            assert resource != null;
            Path path = Paths.get(resource.toURI());
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) exampleText += modifyExampleLine(line);
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(ProtectionOfCropsModule.class.getName()).log(Level.SEVERE, null, ex);
        }

        exampleText += getResultTailWithAddInfo();
        JLabel exampleTextLabel = new JLabel(exampleText);
        exampleTextLabel.setPreferredSize(new Dimension(770,1000));
        panel.add(exampleTextLabel);
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser saveDialog = new JFileChooser();
                saveDialog.showSaveDialog(null);
                File file = saveDialog.getSelectedFile();
                if(file == null) return;
                PrintWriter out = null;
                try {
                    out = new PrintWriter(file);
                    out.println(v1TextField.getText());
                    out.println(v2TextField.getText());
                    out.println(v3TextField.getText());
                    out.println(v4TextField.getText());
                    out.println(p1TextField.getText());
                    out.close();
                }
                catch (FileNotFoundException ex) { }
                finally {
                    if(out != null) out.close();
                }
            }
        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser openDialog = new JFileChooser();
                openDialog.showOpenDialog(null);
                File file = openDialog.getSelectedFile();
                if(file == null) return;
                Scanner sc = null;
                try {
                    sc = new Scanner(file);
                    v1TextField.setText(sc.nextLine());
                    v2TextField.setText(sc.nextLine());
                    v3TextField.setText(sc.nextLine());
                    v4TextField.setText(sc.nextLine());
                    p1TextField.setText(sc.nextLine());
                }
                catch (FileNotFoundException ex) { }
                finally {
                    if(sc != null) sc.close();
                }
            }
        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                v1TextField.setText("0.5");
                v2TextField.setText("0.98");
                v3TextField.setText("0.2");
                v4TextField.setText("0.2");
                p1TextField.setText("0.2");
            }
        };
    }
}
