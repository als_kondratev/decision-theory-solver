package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IEdgeChangeHandler;
import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IValueChangeHandler;

public class Edge extends ListenerHandler<IEdgeChangeHandler> {
    private String  name_;
    private String  description_;
    private Value   value_;
    private double  probability_;
    private boolean preferred_;
    private String id_;

    public Edge (String id, String name, String description, double V, double N, double probability, boolean preferred) {
        name_ = name;
        description_ = description;
        value_ = new Value(V,N);
        probability_ = probability;
        preferred_ = preferred;
        id_ = id;

        value_.addComponent(new IValueChangeHandler() {
            @Override
            public void valueChanged(Value value) {
                for (IEdgeChangeHandler handler : getHandlersList()) {
                    handler.valueChanged(Edge.this);
                }
            }
        });
    }

    public Edge (String id, String name, String description, double V, double N, double propability) {
        this(id, name, description, V, N, propability, false);
    }

    public Edge (String id, String name, String description, double V, double N) {
        this(id, name, description, V, N, 1.0, false);
    }

    public Edge (String id, String name, String description) {
        this(id, name, description, 0.0, 0.0, 1.0, false);
    }

    public String getName () { return name_; }
    public final Value getValue () {
        /*for(IEdgeChangeHandler handle:getHandlersList()){
            handle.valueChanged(this);
        }*/
        return value_;
    }
    public String getId () { return id_; }

    public double getProbability () { return probability_; }
    public void setProbability (double probability) {
        probability_ = probability;
        for(IEdgeChangeHandler handle:getHandlersList()){
            handle.probabilityChanged(this);
        }
    }

    public boolean getPreferred () { return preferred_; }
    public void setPreferred (boolean preferred) {
        preferred_ = preferred;
        for(IEdgeChangeHandler handle:getHandlersList()){
            handle.setPrefered(this);
        }
    }

    public void setValue (Value value){
        value_ = value;
        if (value != null) {
            value.addComponent(new IValueChangeHandler() {
                @Override
                public void valueChanged(Value value) {
                    for (IEdgeChangeHandler handler : getHandlersList()) {
                        handler.valueChanged(Edge.this);
                    }
                }
            });
        }
        for (IEdgeChangeHandler handler : getHandlersList()) {
            handler.valueChanged(this);
        }
    }

    @Override
    public String toString() {
        return getName();
    }

    public String getDescription () { return description_; }

}