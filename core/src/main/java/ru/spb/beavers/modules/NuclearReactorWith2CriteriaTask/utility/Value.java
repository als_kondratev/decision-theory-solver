package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility;

import ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener.IValueChangeHandler;

public class Value extends ListenerHandler<IValueChangeHandler> {
    private double v_;
    private double n_;

    public Value (double v,double n) { v_ = v; n_ = n; }
    public Value (Value value) {this(value.getV(), value.getN());}
    public Value () { this(0.0, 0.0); }

    public double getV() { return v_; }
    public double getN() { return n_; }

    public void setV (double v) {
        v_ = v;
        for (IValueChangeHandler handler: getHandlersList()) {
            handler.valueChanged(this);
        }
    }

    public void setN (double n) {
        n_ = n;
        for (IValueChangeHandler handler: getHandlersList()) {
            handler.valueChanged(this);
        }
    }

    public void add (Value value) {
        v_ += value.getV();
        n_ += value.getN();
        for (IValueChangeHandler handler: getHandlersList()) {
            handler.valueChanged(this);
        }
    }

    public void mul (double constant) {
        v_ *= constant;
        n_ *= constant;
        for (IValueChangeHandler handler: getHandlersList()) {
            handler.valueChanged(this);
        }
    }

    public boolean isComparable(Value value){
        if (v_ == value.getV() && n_ == value.getN()) return false;
        else if (v_ >= value.getV() && n_ <= value.getN()) return false;
        else if (v_ <= value.getV() && n_ >= value.getN()) return false;
        else return true;
    }

    public boolean isBiggerThan(Value value){
        if (v_ > value.getV() && n_ >= value.getN()) return true;
        else if (v_ == value.getV() && n_ > value.getN()) return true;
        else return false;
    }

}