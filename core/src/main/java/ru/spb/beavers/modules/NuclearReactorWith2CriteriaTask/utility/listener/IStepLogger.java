package ru.spb.beavers.modules.NuclearReactorWith2CriteriaTask.utility.listener;

public interface IStepLogger {
    public void logStep(String str);
}