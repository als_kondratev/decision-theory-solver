package ru.spb.beavers.modules.nuclear_reactor;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseGraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.BasicVisualizationServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer;

import javax.swing.*;
import java.awt.*;

/**
 * Created by bazleks on 11.04.2015.
 */
public class Br4FormalPanel extends JPanel {

    Graph<Integer, String> g = new SparseGraph<Integer, String>();

    public Br4FormalPanel() {
        g.addVertex(1);

        g.addEdge("��������� ��������", 1, 2, EdgeType.DIRECTED);
        g.addEdge("�� ��������� ��������", 1, 3, EdgeType.DIRECTED);
        g.addEdge("������� ������� �������", 3, 4);

        Layout<Integer, String> layout = new FRLayout<Integer, String>(g, new Dimension(300, 300));
        layout.setLocation(1, new Point(20, 150));
        layout.lock(1, true);
        layout.setLocation(2, new Point(100, 100));
        layout.lock(2, true);
        BasicVisualizationServer<Integer, String> vv =
                new BasicVisualizationServer<Integer, String>(layout);

        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<Integer>());
        vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller<String>());
        vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR);

        this.add(vv);
    }
}
