package ru.spb.beavers.core.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View краткого описания модуля
 */
public class DescriptionView extends JPanel {

    private final JPanel descriptionPanel;
    private final JButton btnGoMenu;
    private final JButton btnGoTheory;

    public DescriptionView() {
        super(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0;
        constraints.weighty = 0.1;
        constraints.gridwidth = 3;
        constraints.anchor = GridBagConstraints.CENTER;
        this.add(new JLabel(), constraints);

        constraints.fill = GridBagConstraints.NONE;
        btnGoMenu = new JButton("<");
        btnGoMenu.setToolTipText("Назад к выбору задачи");
        constraints.gridwidth = 1;
        constraints.weightx = 0.0;
        constraints.ipady = 22;
        constraints.gridy = 1;
        constraints.insets = new Insets(0, 5, 0, 5);
        this.add(btnGoMenu, constraints);

        btnGoTheory = new JButton(">");
        btnGoTheory.setToolTipText("Теоретическое решение задачи");
        constraints.gridx = 2;
        this.add(btnGoTheory, constraints);

        descriptionPanel = new JPanel(new GridBagLayout());
        JScrollPane scrollPane = new JScrollPane(descriptionPanel);
        scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 1;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        this.add(scrollPane, constraints);

        constraints.gridy = 2;
        constraints.gridx = 0;
        constraints.weighty = 0;
        constraints.gridwidth = 3;
        this.add(new JLabel(), constraints);

        initListeners();
    }

    private void initListeners() {
        btnGoMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                GUIManager.setActiveView(GUIManager.getMenuView());
            }
        });

        btnGoTheory.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                GUIManager.setActiveView(GUIManager.getTheoryView());
            }
        });
    }

    public JPanel getDescriptionPanel() {
        return descriptionPanel;
    }
}
