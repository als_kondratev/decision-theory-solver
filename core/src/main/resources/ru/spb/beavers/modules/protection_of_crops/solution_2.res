<html> 
	<b>Расчет полезностей решений:</b><br/> 
	<p>   
		Ценности исходов (альтернатив) определяются с учетом введенных обозначений как <br/>
		V(y<sub>1</sub>, d<sub>1</sub>) = V(a<sub>1</sub>) = v<sub>1</sub>;<br/>
		V(y<sub>2</sub>, d<sub>1</sub>) = V(a<sub>2</sub>) = v<sub>2</sub>;<br/>
		V(y<sub>1</sub>, d<sub>2</sub>) = V(a<sub>3</sub>) = v<sub>3</sub>;<br/>
		V(y<sub>2</sub>, d<sub>2</sub>) = V(a<sub>2</sub>) = v<sub>2</sub>;<br/><br/>

		Полезность решения d<sub>1</sub> лотереи L<sub>1</sub> равна 
		u(d<sub>1</sub>) = p<sub>1</sub>v<sub>1</sub> + p<sub>2</sub>v<sub>2</sub><br/>

		Полезность решения d<sub>2</sub> лотереи L<sub>2</sub> равна 
		u(d<sub>2</sub>) = p<sub>1</sub>v<sub>3</sub> + p<sub>2</sub>v<sub>4</sub>

	</p>   
	<b>Выбор решения:</b><br/> 
	<p>   
		<i>Байесовский подход к выбору решения.</i><br/>
		В условиях наличия двух альтернатив лучшее в байесовском смысле решение находится путем 
		сравнения соответствующих полезностей: <br/>
		d<sup>*</sup> = d<sub>1</sub>, если u(d<sub>1</sub>) ≥ u(d<sub>2</sub>), иначе d<sup>*</sup> = d<sub>2</sub><br/> 

		С учетом полученных значений полезности решений получаем: 
		d<sup>*</sup> = d<sub>1</sub>, если p<sub>1</sub>(v<sub>1</sub> - v<sub>3</sub>) ≥ p<sub>2</sub>(v<sub>4</sub> - v<sub>2</sub>), 
		иначе d<sup>*</sup> = d<sub>2</sub><br/> 
	</p> 
	<b>Вероятность безразличия:</b><br/> 
	<p>   
		Вероятность безразличия p&#772; - значение p<sub>1</sub>, при котором принятое
		решение не оказывает влияния на математическое ожидание получаемого дохода.<br/>
		p&#772;(v<sub>1</sub> - v<sub>3</sub>) = (1 - p&#772;)(v<sub>4</sub> - v<sub>2</sub>)
		откуда p&#772; = (v<sub>4</sub> - v<sub>2</sub>) / (v<sub>1</sub> - v<sub>3</sub> + v<sub>4</sub> - v<sub>2</sub>)<br/>

		С учетом вероятности безразличия Байесовское решение принимает вид: 
		d<sup>*</sup> = d<sub>1</sub>, если p<sub>1</sub> ≥ p&#772;, иначе d<sup>*</sup> = d<sub>2</sub><br/> <br/> 
		<i>Осторожное решение.</i><br/>
		<i>Осторожный подход к выбору решения.</i><br/>
		Поскольку доход v<sub>1</sub> по условию задачи больше дохода v<sub>3</sub> , осторожное решение есть δ<sup>*</sup> = d<sub>1</sub>. <br/>
		Полезность осторожного решения u(δ<sup>*</sup>) = p<sub>1</sub>v<sub>1</sub> + (1 - p<sub>1</sub>)v<sub>2</sub>
	</p> 
</html>